package com.atguigu;

/**
 * @author songfei
 * @create 2022/1/23 15:53
 */
public class UserService {

    private String userName;
    private String passwordMaster;
    private String email;
    private float score;

    public UserService() {
    }

    public UserService(String userName, String passwordMaster, String email, float score) {
        this.userName = userName;
        this.passwordMaster = passwordMaster;
        this.email = email;
        this.score = score;
    }
}

